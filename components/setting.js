// components/dashboard.js

import React, { Component } from 'react';
import { StyleSheet, View, Text, Button } from 'react-native';
import firebase from '../database/firebase';

export default class Dashboard extends Component {
  constructor() {
    super();
  }

  signOut = () => {
    firebase.auth().signOut().then(() => {
      console.log("signout");
      this.props.navigation.navigate('Login');
    })
    .catch(error => this.setState({ errorMessage: error.message }))
  }  

  render() { 
    return ( 
        <Button
          color="#3740FE"
          title="Logout"
          onPress={() => this.signOut()}
        /> 
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    justifyContent: 'center',
    alignItems: 'center',
    padding: 35,
    backgroundColor: '#fff'
  },
  textStyle: {
    fontSize: 15,
    marginBottom: 20
  }
});