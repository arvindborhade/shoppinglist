import React, {useState} from 'react';
import {View,Text, StyleSheet, TouchableOpacity,TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { Button } from 'react-native-paper';
import axios from 'axios';

const AddItem = ({title,addItem}) => { 
  const [text,setText] = useState('');
   
  const onChange = textValue => {
    setText(textValue); 
  }

  const clearValue = textValue =>{
    if(textValue){
        this.textInput.clear();
        setText("");
    } 
  }
  return(
   <View>
       <TextInput 
       placeholder="Add Item ..." 
       style={styles.input}
       onChangeText={onChange}
       ref={input => { this.textInput = input }}
       />
       <TouchableOpacity style={styles.btn} onPress={()=>{addItem(text); clearValue(text)}}>
        {/* <Text style={styles.btnText} ><Icon name='plus' size={20}/> Add Item</Text> */}
        <Button icon="plus" mode="contained" onPress={()=>{addItem(text); clearValue(text)}}>
         Add Item
        </Button>
       </TouchableOpacity>
   </View>
  );
};  



const styles = StyleSheet.create({
    input:{
        height: 60,
        padding:8,
        fontSize:16,
    },
    btn:{ 
        padding:9,
        margin:5,
    },
    btnText:{
        color: 'darkslateblue',
        fontSize:20,
        textAlign: 'center',
    },
});

export default AddItem;