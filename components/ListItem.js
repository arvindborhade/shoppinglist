import React from 'react';
import {View,Text, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';


const listItemText = (status) =>{
    if(status==true){
        return { 

            paddingLeft:15,
            //textAlign:"left",
            fontSize: 18, 
            color:"black",
            textDecorationLine: 'line-through'
        }
    }else{
        return {
            name:'check-square',
            paddingLeft:10,
            //textAlign:"left",
            fontSize: 18, 
            color:"black"
        }
    } 
  }

const ListItem = ({item,deleteItem,markDone}) => { 
  return(
    <TouchableOpacity style={styles.ListItem}>
        <View style={styles.listItemView}>
            <Icon 
            name="check-square-o" 
            size={20} 
            color="green" 
            onPress={()=> markDone(item.id,item.status) } 
            />
            <Text style={listItemText(item.status)} >
                {item.item}
            </Text> 

            <Icon 
            style={styles.listItemIcon}
            name="remove" 
            size={20} 
            color="firebrick" 
            onPress={()=>deleteItem(item.id)}
            />
        </View>
    </TouchableOpacity>
  );
};  



const styles = StyleSheet.create({
    ListItem:{
        padding:15,
        backgroundColor:"white",
        borderBottomWidth: 0.5,
        borderColor:"#4A4AFF",
    },
    listItemView:{
         flexDirection:'row',
         //alignItems:'center',
         paddingLeft:5,
         justifyContent:'space-between',
        // justifyContent:'space-between',
        // alignItems:'center'
    },
   
    listItemIcon:{
        paddingRight:20,       
        fontSize: 18,   
        //alignSelf: 'flex-end', 
        color:'red',  
    }
});

export default ListItem;