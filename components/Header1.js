
import * as React from 'react';
import { Appbar } from 'react-native-paper';



_goBack = () => console.log('Went back');

_handleSearch = () => console.log('Searching');

_handleMore = () => console.log('Shown more');

const Header1 = ({title}) => { 
  return(
    <Appbar.Header>
    <Appbar.BackAction
      onPress={this._goBack}
    />
    <Appbar.Content
      title="Shopping List"
    />
    <Appbar.Action icon="magnify" onPress={this._handleSearch} />
    <Appbar.Action icon="dots-vertical" onPress={this._handleMore} />
    </Appbar.Header>
    
    
  );
};  



export default Header1;