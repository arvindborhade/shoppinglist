import React, {useState} from 'react';
import {View,Text, StyleSheet,FlatList, Alert, Button} from 'react-native';
import { List } from 'react-native-paper';
import Header from './Header';
import Header1 from './Header1';
import ListItem from './ListItem';
import AddItem from './AddItem';
import axios from 'axios';
import Login from './login';
import mailDashboard from './mainDashboard'




// import { Button, Paragraph, Menu, Divider, Provider } from 'react-native-paper'; 
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import firebase from '../database/firebase';
import { AsyncStorage } from 'react-native';

const Stack = createStackNavigator();



//import { uuid } from 'uuidv4';

//const apiurl = "https://uubayqyu8c.execute-api.ap-south-1.amazonaws.com/dev/";
const apiurl = "http://192.168.0.111:3000/";

const App = ({route,navigation}) => {

  const Listid = route.params.id; 
  
  // const [items, setItems] = useState([
  //   {id:1, text:"Milk"},
  //   {id:2, text:"Eggs"},
  //   {id:3, text:'Bread'},
  //   {id:4, text:'Juice'}
  // ]);

  //firebase.auth().currentUser ? firebase.auth().currentUser.email : "unknown user"


  if(!Listid){
    navigation.navigate('mailDashboard');
  }

  const [uid, setUid] = useState([]);
  const [getlistcount, setGetlistcount] = useState([]);

  let uidFirebase = firebase.auth().currentUser? firebase.auth().currentUser.uid:"unknown";
 
  AsyncStorage.getItem('uid').then((value) => {

    if(value=="unknown" || uid =="unknown"){ 
      
      AsyncStorage.setItem('uid', uidFirebase);

    }else{
      setUid(value)
    } 
  });



    

  AsyncStorage.getItem('uid').then((value) => setUid(value));
  
  console.log("List Id:"+Listid);

    

  const [items, setItems] = useState([]);
 
  const getdatalist = () =>{

      axios.post(apiurl+"getItems", {
        uid: firebase.auth().currentUser.uid,
        listid: Listid
      })
      .then(function (response) {
        var json = response.data;
        console.log(json);
        setItems(json);   
      })
      .catch(function (error) { 
        console.log(error);
     }); 
  }
 
  let myinterval;
 
    if(getlistcount){ 
      setGetlistcount(0); 
     // clearInterval(myinterval);
      
      setTimeout(() => { 
 
        if(firebase.auth().currentUser){
          getdatalist(); 
        } 
       },0); 
    }
 
  

  const deleteItem = (id) => {

    axios.post(apiurl+"deleteItem", {
      itemId: id
    })
    .then(function (response) {
      console.log(response.data);
     
    })
    .catch(function (error) {
      //console.log("Reach Here Error");
      console.log(error);
    });

    
    setItems(prevItems=>{
      return prevItems.filter(item=>item.id != id);
    })
  }

  const markDone = (id,status) => {

    if(status==true){
      status=false;
    }else{
      status=true;
    }

    axios.post(apiurl+"markDone", {
      itemId: id,
      status: status,
    })
    .then(function (response) {
      console.log("Reach Here Error");
      console.log(response.data);
      getdatalist();
    })
    .catch(function (error) {
      //console.log("Reach Here Error");
      console.log(error);
    });

    
    
  }

  const addItem = (text) => {
    
    if(!text){
     
      Alert.alert(
        'Error',
        'Please Enter Item',
        [ 
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        {cancelable: false},
      );
      
    }else{ 
      // add item
      console.log("Reach Here"); 
     
      // axios.get(url).then(function(response){
      //   alert(response);
      // }).catch(function(error){
      //   alert(error);
      // });
      axios.post(apiurl+"addItem", {
          uid: firebase.auth().currentUser.uid,
          item: text,
          listid:Listid
        })
        .then(function (response) {
          console.log(response.data);
          getdatalist();
        })
        .catch(function (error) {
          //console.log("Reach Here Error");
          console.log(error);
        });
        
 
      // setItems(prevItems=>{
      //   return [{id:12, item:text}, ...prevItems];
      // });
    }
  
  }
  
 
  return( 
    <View style={styles.container}> 
      <AddItem addItem={addItem}/>  
      <FlatList  
        data={items}
        renderItem = {({item}) => <ListItem item={item} deleteItem={deleteItem} markDone={markDone} /> }
      />   
      
   </View> 
  ); 
};

const styles = StyleSheet.create({
  container:{
    flex:1,
  }, 
});

export default App;
