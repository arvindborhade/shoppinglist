import React, {
  useState
} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Alert,
  Button
} from 'react-native';
import ListItem from './mainlist';
import AddList from './AddList';
import axios from 'axios';


// import { Button, Paragraph, Menu, Divider, Provider } from 'react-native-paper'; 
import {
  NavigationContainer
} from '@react-navigation/native';
import {
  createStackNavigator
} from '@react-navigation/stack';
import firebase from '../database/firebase';
import {
  AsyncStorage
} from 'react-native';

const Stack = createStackNavigator();



//const apiurl = "https://uubayqyu8c.execute-api.ap-south-1.amazonaws.com/dev/";
const apiurl = "http://192.168.0.111:3000/";

const App = ({ route, navigation }) => {


    
    console.log(route.params);
      // const [items, setItems] = useState([
      //   {id:1, text:"Milk"},
      //   {id:2, text:"Eggs"},
      //   {id:3, text:'Bread'},
      //   {id:4, text:'Juice'}
      // ]);

      //firebase.auth().currentUser ? firebase.auth().currentUser.email : "unknown user"


      const [uid, setUid] = useState([]);
      const [getlistcount, setGetlistcount] = useState([]);

      let uidFirebase = firebase.auth().currentUser ? firebase.auth().currentUser.uid : "unknown";

      AsyncStorage.getItem('uid').then((value) => {

          if (value == "unknown" || uid == "unknown") {

              AsyncStorage.setItem('uid', uidFirebase);

          } else {
              setUid(value)
          }
      });




      AsyncStorage.getItem('uid').then((value) => setUid(value));

      console.log(uid);



      const [items, setItems] = useState([]);

      const getdatalist = () => {

          axios.post(apiurl + "getList", {
                  uid: firebase.auth().currentUser.uid,
              })
              .then(function(response) {
                  var json = response.data;
                  console.log(json);
                  setItems(json);
              })
              .catch(function(error) {
                  console.log(error);
              });
      }

      let myinterval;
      //const getItems = () => { 
      if (getlistcount) {
          setGetlistcount(0);
          // clearInterval(myinterval);

          setTimeout(() => {

              if (firebase.auth().currentUser) {
                  getdatalist();
              }
          }, 4000);

      }

      //} 

      // done
      const markDone = (id, status) => {

          if (status == true) {
              status = false;
          } else {
              status = true;
          }

          axios.post(apiurl + "markDoneList", {
                  itemId: id,
                  status: status,
              })
              .then(function(response) {
                  console.log("Reach Here Error");
                  console.log(response.data);
                  getdatalist();
              })
              .catch(function(error) {
                  //console.log("Reach Here Error");
                  console.log(error);
              });
      }


      // done
      const deleteList = (id) => {

          axios.post(apiurl + "deleteList", {
                  itemId: id
              })
              .then(function(response) {
                  console.log(response.data);
              })
              .catch(function(error) {
                  //console.log("Reach Here Error");
                  console.log(error);
              });

          setItems(prevItems => {
              return prevItems.filter(item => item.id != id);
          })
      }

      //done
      const addList = (text) => {

          if (!text) {

              Alert.alert(
                  'Error',
                  'Please Enter Item',
                  [{
                      text: 'OK',
                      onPress: () => console.log('OK Pressed')
                  }, ], {
                      cancelable: false
                  },
              );

          } else {
              // add item
              console.log("Reach Here");
              // axios.get(url).then(function(response){
              //   alert(response);
              // }).catch(function(error){
              //   alert(error);
              // });
              axios.post(apiurl + "addlist", {
                      uid: firebase.auth().currentUser.uid,
                      list: text
                  })
                  .then(function(response) {
                      getdatalist();
                      // setItems(prevItems=>{
                      //     return [{id:response.data.id, list:text}, ...prevItems];
                      // });
                      console.log(response.data);
                  })
                  .catch(function(error) {
                      //console.log("Reach Here Error");
                      console.log(error);
                  });
          }



      }


      // const signOut = () => {
      //   firebase.auth().signOut().then(() => {

      //     console.log("signout");
      //     clearInterval(myinterval);

      //     let keys = ["uid"];
      //     AsyncStorage.multiRemove(keys, (err) => {
      //       console.log('Local storage user info removed!');
      //     })

      //     navigation.navigate('Login');

      //   })
      //   .catch(error => this.setState({ errorMessage: error.message }))
      // }  


      const redirectto = (id) => {
          // navigate to dashboard 
          console.log(id);
          navigation.navigate('Dashboard', {
              id: id
          });
      }

      return ( 
            <View style = { styles.container } >
            <AddList addList = { addList }  />   
            <FlatList data = { items }
            renderItem = { ({  item  }) => <ListItem item = { item } deleteList = {  deleteList  } markDone = { markDone } redirectto = { redirectto } /> } />
            </View> 
          );
      };


      const styles = StyleSheet.create({
          container: {
              flex: 1,
          },
      });

      export default App;