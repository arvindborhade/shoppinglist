// components/dashboard.js

import React, { Component } from 'react';
import { StyleSheet, View, Text, Button, Image } from 'react-native';
import firebase from '../database/firebase';
import { AsyncStorage } from 'react-native';





//https://cdn.dribbble.com/users/1671186/screenshots/7103694/shoppinh-online---isometric.gif
export default class Dashboard extends Component {
  constructor() {
    super();
    this.state = { 
      uid: ''
    }
    AsyncStorage.getItem('uid').then((value) =>{ 
      //setUid(value) 
      if(value!='unknown'){
        console.log("Send to DashBoard");
        this.props.navigation.navigate('mainDashboard');
      }  
    }); 
  }

  
  


  uidFirebase = firebase.auth().currentUser? firebase.auth().currentUser.uid:"unknown";

  checkifLogin=()=>{

    uidFirebase = firebase.auth().currentUser? firebase.auth().currentUser.uid:"unknown";

    if(uidFirebase!="unknown"){
      this.props.navigation.navigate('mainDashboard');
    }else{
      this.props.navigation.navigate('Signup');
    }
  }

  render() { 

      setTimeout(() => {
          this.checkifLogin()
      }, 0);
   
    
    return ( 
      <View style={styles.container}>
        {/* <Image source={{uri : 'https://cdn.dribbble.com/users/1671186/screenshots/7103694/shoppinh-online---isometric.gif'}} style = {{width: 315, height: 383}} /> */}

        <Image source={require('../images/logo.png')}  style = {{width: 250, height: 250}} />
        
         <Text
          color="Blue"
          title="Logout" 
          style={styles.textStyle}
        > Welcome To Shopping List </Text>

        <Text
          color="Blue"
          title="Logout" 
          style={styles.loadingtextStyle}
        > Loading... </Text>

        
      </View>
       
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    justifyContent: 'center',
    alignItems: 'center',
    padding: 35,
    backgroundColor: '#fff'
  },
  textStyle: {
    fontSize: 25,
    paddingTop: 25,
    marginBottom: 20
  },
  loadingtextStyle: {
    fontSize: 15,
    paddingTop: 25,
    marginBottom: 20
  }
});