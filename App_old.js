import React, {useState} from 'react';
import {View,Text, StyleSheet,FlatList, Alert,Button} from 'react-native';

// import { Button, Paragraph, Menu, Divider, Provider } from 'react-native-paper'; 
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';

import Login from './components/login';
import Signup from './components/signup';
import Dashboard from './components/dashboard';
import mainDashboard from './components/mainDashboard';
import Splash from './components/splash';
import Setting from './components/setting';
 


import Icon from 'react-native-vector-icons/dist/FontAwesome';

const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator();


const [userToken, setUserToken] = useState(null);


const HomeIconWithBadge = (props) => {
  // You should pass down the badgeCount in some other ways like React Context API, Redux, MobX or event emitters.
  return <IconWithBadge {...props} badgeCount={3} />;
}


const IconWithBadge = ({ name, badgeCount, color, size }) => {
  return (
    <View style={{ width: 24, height: 24, margin: 5 }}>
      <Ionicons name={name} size={size} color={color} />
      {badgeCount > 0 && (
        <View
          style={{
            // On React Native < 0.57 overflow outside of parent will not work on Android, see https://git.io/fhLJ8
            position: 'absolute',
            right: -6,
            top: -3,
            backgroundColor: 'red',
            borderRadius: 6,
            width: 12,
            height: 12,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Text style={{ color: 'white', fontSize: 10, fontWeight: 'bold' }}>
            {badgeCount}
          </Text>
        </View>
      )}
    </View>
  );
}

function MyStack({navigation}){
  
  return(
    <Stack.Navigator
      initialRouteName="Splash"
      screenOptions={{
        headerTitleAlign: 'center',
        headerStyle: {
          backgroundColor: '#3740FE',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          
        },
      }}>

      <Stack.Screen 
       name="Shopping List" 
       component={Splash} 
       options={
         { title: 'Shopping List' },
         {headerLeft: null} 
       }
      /> 

      <Stack.Screen 
        name="Signup" 
        component={Signup} 
        options={{ title: 'Signup' }, {headerLeft: null} }
      />  

      <Stack.Screen 
        name="Login" 
        component={Login} 
        options={
          {title: 'Login'},
          {headerLeft: null} 
        }
      /> 
      <Stack.Screen 
       name="mainDashboard" 
       component={mainDashboard} 
       options={{
        title: "List Items",
        headerRight: () => ( 
          
          <Icon 
          style={{paddingEnd:20}}
          name="gear" 
          size={20} 
          color="white" 
          onPress={() => navigation.navigate('Setting')}
          />
         
        ),
        headerLeft: null
      }} 
        
      />
      <Stack.Screen 
       name="Dashboard" 
       component={Dashboard} 
       options={{
        title: "List Items",
        headerRight: () => ( 
          
          <Icon 
          style={{paddingEnd:20}}
          name="gear" 
          size={20} 
          color="white"  
          onPress={() => props.navigation.navigate('Setting')}
          />
         
        ),
      }} 
      /> 

      <Stack.Screen 
       name="Setting" 
       component={Setting} 
       options={{
       title: "Settings", 
      }} 
      />
      
    </Stack.Navigator>
  );

}


const AuthStack = createStackNavigator();
const AuthStackScreen = () => (
  <AuthStack.Navigator>

      <AuthStack.Screen 
        name="Signup" 
        component={Signup} 
        options={{ title: 'Signup' }, {headerLeft: null} }
      />  

      <AuthStack.Screen 
        name="Login" 
        component={Login} 
        options={
          {title: 'Login'},
          {headerLeft: null} 
        }
      /> 
  </AuthStack.Navigator>
);



const RootStack = createStackNavigator();
const RootStackScreen = ({ userToken }) => (
  <RootStack.Navigator headerMode="none">
    {userToken ? (
      <RootStack.Screen
        name="App"
        component={DrawerScreen}
        options={{
          animationEnabled: false
        }}
      />
    ) : (
      <RootStack.Screen
        name="Auth"
        component={AuthStackScreen}
        options={{
          animationEnabled: false
        }}
      />
    )}
  </RootStack.Navigator>
);


let getlistcount = 1;

const apiurl = "https://uubayqyu8c.execute-api.ap-south-1.amazonaws.com/dev/";

const App = () => {  
  return( 
    <NavigationContainer>    
     <Tabs.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            if (route.name === 'Home') {
              return (
                <HomeIconWithBadge
                  name={
                    focused
                      ? 'ios-information-circle'
                      : 'ios-information-circle-outline'
                  }
                  size={size}
                  color={color}
                />
              );
            } else if (route.name === 'Settings') {
              return (
                <Icon 
              style={{paddingEnd:20}}
              name="gear" 
              size={20} 
              color="black"   
              />
              
               
              );
            }
          },
        })}
        tabBarOptions={{
          activeTintColor: 'tomato',
          inactiveTintColor: 'gray',
        }}
      >
        <Tabs.Screen name="Home" component={MyStack} />
        <Tabs.Screen name="Setting" component={Setting} />
      </Tabs.Navigator> 
    </NavigationContainer>
  ); 
};
 
const styles = StyleSheet.create({
  container:{
    flex:1,
  }, 
});

export default App;
