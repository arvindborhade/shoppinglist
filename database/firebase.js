// database/firebaseDb.js
import * as firebase from "firebase";


const firebaseConfig = {
    apiKey: "AIzaSyAVfNX2AajH2bqxf3zjfzA5rkiL8R8h5CQ",
    authDomain: "shoppinglist-d6e74.firebaseapp.com",
    databaseURL: "https://shoppinglist-d6e74.firebaseio.com",
    projectId: "shoppinglist-d6e74",
    storageBucket: "shoppinglist-d6e74.appspot.com",
    messagingSenderId: "834311091415",
    appId: "1:834311091415:android:c52d6a5daab1b218296c92" 
  };


firebase.initializeApp(firebaseConfig); 
export default firebase;

